#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>


// Function prototypes
void process(const int n);
void process2();


int main(){

    process(4);
    process2();

    return 0;
}


void process(const int n) {

    printf(".:: Process # Launch of %d processes ::.\n\n", n);
    printf("Father PPID %d <────┐\n", getpid());
    printf("                      |\n");

    for (int i = 1; i <= n; ++i) {

        if (fork() == 0){
            printf("%d : Son PID %d => %d\n", i, getpid(), getppid());
            exit(0);
        }

        wait(NULL); // Synchronizes processes (Thread)
    }

    printf("\n--------------------------------------\n\n");
}


void process2() {

    const char * word[] = {"Hello", "Test", "Test2", "Bye"};
    const int sizeWord = (sizeof(word) / sizeof(word[0]));

    printf(".:: Process2 # Launch of %d processes ::.\n\n", sizeWord);
    printf("Father PPID %d <────┐\n", getpid());
    printf("                      |\n");

    for (int i = 1; i <= sizeWord; ++i) {

        if (fork() == 0){
            printf("%d : Son PID %d => %d # %s\n", i, getpid(), getppid(), word[(i-1)]);
            exit(0);
        }

        wait(NULL); // Synchronizes processes (Thread)
    }

    printf("\n--------------------------------------\n\n");

}